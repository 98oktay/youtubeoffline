var stringSimilarity = require("string-similarity");

var items = [
    {title: 'KİM DAHA İNSAN ? | DETROIT BECOME HUMAN TÜRKÇE BÖLÜM 11'},
    {title: 'ARTIK GERİ DÖNÜŞ YOK ! | DETROIT BECOME HUMAN TÜRKÇE BÖLÜM 14'},
    {title: 'GELECEĞE HOŞ GELDİNİZ ! | DETROIT BECOME HUMAN TÜRKÇE BÖLÜM 1'},
    {title: 'ENFEKSİYONUN KALBİ ! ! STATE OF DECAY 2 TÜRKÇE'},
    {title: 'KORSANLAR ! | DETROIT BECOME HUMAN TÜRKÇE BÖLÜM 10'},
    {title: 'BÜYÜK KARARLAR ! | DETROIT BECOME HUMAN TÜRKÇE BÖLÜM 13'},
    {title: 'ARTIK GERİ DÖNÜŞ YOK ! | DETROIT BECOME HUMAN TÜRKÇE BÖLÜM 15'},
    {title: 'DİRENİŞ BAŞLASIN ! | DETROIT BECOME HUMAN TÜRKÇE BÖLÜM 8'},
    {title: 'KÜLLERDEN DOĞUŞ ! | DETROIT BECOME HUMAN TÜRKÇE BÖLÜM 5'},
    {title: 'ÖLÜM VE SİS ! | GOD OF WAR PS4 TÜRKÇE Bölüm 34'},
    {title: 'GALİBA ÖĞRENİYORUM! | FORTNITE BATTLE ROYALE'},
    {title: 'JASON GERİ DÖNDÜ ! | FRIDAY 13th THE GAME'},
    {title: 'HENRY\'NİN KADİM GÜCÜ ! Kingdom Come Deliverance Türkçe Bölüm 15'},
    {title: 'KADİM SAVAŞÇILAR ! VALKÜR ! | GOD OF WAR PS4 TÜRKÇE Bölüm 30'},
    {title: 'LOKI VE THOR ! | GOD OF WAR PS4 TÜRKÇE Bölüm 27'},
    {title: 'SON ŞEHİR ! | FROSTPUNK TÜRKÇE Bölüm 3'},
    {title: 'Anlamayan Kalmasın #7 Mayoz Bölünme'},
    {title: 'Sınavdan Önce Çözmeniz Gereken 100 Soru #8'},
    {title: 'Sınavdan Önce Çözmeniz Gereken 100 Soru #7'},
    {title: 'Anlamayan Kalmasın #3 Kalbin Yapısı ve Dolaşım Sistemi'},
    {title: 'Sınavdan Önce Çözmeniz Gereken 100 Soru #1'},
    {title: 'Sınavdan Önce Çözmeniz Gereken 100 Soru #4'},
    {title: 'Sınavdan Önce Çözmeniz Gereken 100 Soru #3'},
    {title: 'Anlamayan Kalmasın #2 Fotosentez'},
    {title: 'Sınavdan Önce Çözmeniz Gereken 100 Soru #6'},
    {title: 'Anlamayan Kalmasın #4 Oksijenli Solunum'},
    {title: 'Sınavdan Önce Çözmeniz Gereken 100 Soru #5'},
    {title: 'Anlamayan Kalmasın #6 Kulağın Yapısı'},
    {title: 'Sınavdan Önce Çözmeniz Gereken 100 Soru #2'},
    {title: 'YAŞAMAK MI YAŞATMAK MI ? | DETROIT BECOME HUMAN ALTERNATİF SONLAR BÖLÜM 2'},
    {title: 'ALTERNATİF TERCİHLER ! | DETROIT BECOME HUMAN ALTERNATİF SONLAR BÖLÜM 1'},
    {title: 'FİLM TADINDA FİNAL ! | FROSTPUNK TÜRKÇE Bölüm 4'},
    {title: 'FARKLI YOLLAR ! | DETROIT BECOME HUMAN ALTERNATİF SONLAR BÖLÜM 3'},
    {title: 'YENİ OPERATÖRLER,TÜRKÇE OYUN VE TÜRK OPERATÖRLER ! | Tom Clancy’s Rainbow Six Siege'},
    {title: 'KRALİÇEYE GİDEN YOL ! | GOD OF WAR PS4 TÜRKÇE Bölüm 33'}
];


function groupItems(items) {

    var groups = {};

    // numeric order
    items.sort(function (a, b) {

        var title_A = a.title;
        var title_B = b.title;

        if (/\d+/.test(title_A) && /\d+/.test(title_B)) {
            var num_A = title_A.match(/(\d+)/gi);
            var num_B = title_B.match(/(\d+)/gi);

            if (num_A) num_A = parseInt(num_A.pop(), 10);
            if (num_B) num_B = parseInt(num_B.pop(), 10);

            return num_B - num_A;
        }
        return 1;
    });

    // Similarity control
    for (var i = 0; i < items.length; i++) {
        var a = items[i];
        for (var j = 0; j < items.length; j++) {
            var b = items[j];

            var title_A = a.title;
            var title_B = b.title;

            var rate = stringSimilarity.compareTwoStrings(title_A, title_B);
            if (rate > 0.5 && title_A !== title_B && !b.inGroup) {
                if (groups[title_A]) {
                    groups[title_A].push(b);
                    b.inGroup = title_A;
                    b.rate = rate
                } else if (groups[title_B] && !a.inGroup) {
                    groups[title_B].push(a);
                    a.inGroup = title_B;
                    a.rate = rate
                } else {
                    var groupname = title_A;
                    groups[groupname] = [a, b];
                    a.inGroup = groupname;
                    b.inGroup = groupname;
                    a.rate = 1;
                    b.rate = rate
                }
            }
        }
    }

    // Out group
    for (var k = 0; k < items.length; k++) {
        var c = items[k];
        if (!c.inGroup) {
            if (!groups['others']) {
                groups['others'] = [c]
            } else {
                groups['others'].push(c);
            }
        }

    }

    return groups;
}

var start_date = new Date();
var result = groupItems(items);

var end_date = new Date();

console.log(JSON.stringify(result, null, 4));

console.log("proccess ms:", end_date - start_date);