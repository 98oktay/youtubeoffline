import React, {Component} from "react";
import {Actions,Modal, Scene, Router} from 'react-native-router-flux';
import Home from "./Scenes/Home";
import Player from "./Scenes/PlayerTest";
console.disableYellowBox = true;

const scenes = Actions.create(
    <Scene key="root" >
        <Scene key="home" hideNavBar={true} panHandlers={null} component={Home} title="Home" inital/>
        <Scene key="player" hideNavBar={true} component={Player} title="Offlayn TV"/>
    </Scene>
);

export default class extends Component {
    render() {
        return <Router scenes={scenes}/>
    }
}
