/**
 * Created by Oktay on 9/24/2017.
 */

import React, {Component} from "react";
import {Platform, Text, TouchableOpacity, View, Alert} from "react-native";
import Downloader from "../Utilies/Downloader";
import * as Progress from 'react-native-progress';
import styleVideos from "../Styles/videos";
import Shared from "../Utilies/Shared";
import Icon from 'react-native-vector-icons/Ionicons';


export default class extends Component {

    constructor(props) {
        super(props);
        this.state = {
            downloaded: false,
            status: '',
            info: props.info,
            progress: {},

        };

    }

    componentWillMount() {
        Downloader.isDownloaded(this.state.info, (state) => {
            this.setState({downloaded: state});
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            info: nextProps.info
        })

    }

    downloadVideo(force = false) {
        if (!force && Shared.connectionType === "cellular") {
            return Alert.alert(
                'Hücresel Bağlantı Kullanıyorsunuz',
                'İçerik indirmeyi gerçekten istiyor musunuz?',
                [
                    {text: 'İptal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'Devam Et', onPress: () => this.downloadVideo(true)},
                ],
                {cancelable: false}
            )
        }

        this.setState({status: 'loading', jobId: ''});

        Downloader.offline(this.props.info,
            (err) => {
                if(err && err.code === "COPYRIGHT_REASON") {
                    Alert.alert("Hata","Bu içeriğin telif hakkı bulunduğundan indirme işlemine izin verilemez");

                }else if(err){
                    Alert.alert("Hata","Aşağıdaki sebepten dolayı hata oluştu: \n" + JSON.stringify(err));

                }
                this.setState({status: 'error'});
            },
            (info) => {
                this.setState({status: 'started', jobId: info.jobId})
            },
            (progressData) => {
                this.setState({progress: progressData, status: 'downloading'})
                Shared.onProgressUpdate(progressData, progressData.jobId)
            },
            (info) => {
                this.setState({progress: '', jobId: '', downloaded: true, status: 'completed'});
                Shared.onProgressFinish(info.jobId);
                Shared.reloadLibrary();
            },
        )
    }

    removeVideo(forced = false) {
        if (!forced) {

            return Alert.alert(
                'Emin misiniz?',
                'Kütüphaneden kalıcı olarak kaldırılacak',
                [
                    {text: 'İptal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'Kaldır', onPress: () => this.removeVideo(true)},
                ],
                {cancelable: false}
            );

            return false;
        }
        Downloader.removeVideo(this.props.info, () => {
            this.setState({downloaded: false});
            Shared.reloadLibrary();
        });
    }

    cancelDownload(forced) {

        if (!forced) {

            return Alert.alert(
                'Emin misiniz?',
                'İndirme işlemi durdurulsun mu?',
                [
                    {text: 'Devam et', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'Durdur', onPress: () => this.cancelDownload(true)},
                ],
                {cancelable: false}
            );

            return false;
        }

        Downloader.stopDownload(this.state.jobId);

        Downloader.removeVideo(this.props.info, () => {});

        this.setState({downloaded: false, jobId:'', status:''});

        Shared.reloadLibrary();

    }


    render() {
        return (<View style={styleVideos.downloadWrapper}>
            {this.state.status === 'downloading' ?
                <TouchableOpacity onPress={() => {
                    this.cancelDownload();
                }} style={{zIndex:10}}>
                    <Icon
                        style={{marginTop: -5, marginRight: 5}}
                        color="#FF7A00"
                        name="ios-alert"
                        size={24}
                    />
                </TouchableOpacity> : this.state.downloaded ?
                    <TouchableOpacity style={styleVideos.removeButton} onPress={() => {
                        this.removeVideo()
                    }}>
                        <Icon
                            style={{marginTop: -12, marginBottom: -12}}
                            color="#007AFF"
                            name="ios-checkmark-circle-outline"
                            size={36}
                        />

                    </TouchableOpacity> : <TouchableOpacity onPress={() => {
                        this.downloadVideo()
                    }}>
                        <Icon
                            style={{ marginRight: 5}}
                            color="#007AFF"
                            name="ios-cloud-download"
                            size={24}
                        />
                    </TouchableOpacity>

            }
            <View style={{position: 'absolute', right: 0, bottom: 0}}>
                {this.state.status === 'loading' &&
                <Progress.Circle size={30} indeterminate={true}/>
                }
                {this.state.status === 'downloading' &&
                <Progress.Circle size={30} showsText={false}
                                 progress={(this.state.progress.bytesWritten / this.state.progress.contentLength)}/>
                }
            </View>
        </View>);
    }
}