import React, {Component, Fragment} from "react";
import {Alert, Text, TouchableOpacity} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import Shared from "../Utilies/Shared";
import FadeInView from "react-native-fade-in-view";
import RNFS from "react-native-fs";
import moment from "moment/moment";
import TimeAgo from 'react-native-timeago';
import {Actions} from "react-native-router-flux";

require('moment/locale/tr');
moment.locale('tr-TR');

export default class extends Component {

    constructor(props) {
        super(props);
        this.videoId = props.video.id.videoId ? props.video.id.videoId : props.video.id;
        this.state = {
            finishDate: 0,
            textStyles:{}
        };


    }

    componentWillMount() {
        Shared.getVideoInfo(this.videoId, (info) => {
            this.setState(info);
        });
    }

    removeVideo(data, forced) {

        if (!forced) {

            let buttons = [
                {text: 'Şimdi Sil', onPress: () => this.removeVideo(data, true)},
                {text: 'İptal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'}
            ];

            if(this.state.finishDate>0) {
                buttons.push({text: 'Silmekten Vazgeç', onPress: () => {
                        Shared.getVideoInfo(this.videoId, (info) => {
                            info.finishDate = -1;
                            this.setState(info);
                            Shared.setVideoInfo(this.videoId, info, ()=>{
                                Shared.callExpiryWorkUpdater(this.videoId);
                            });
                        });
                    }});
            }

            return Alert.alert(
                'Emin misiniz?',
                'Kütüphaneden kalıcı olarak kaldırılacak',
                buttons,
                {cancelable: false}
            );

            return false;
        }


        let metaFile = data.offline.replace(/\.mp4/i, '.json');
        let thumFile = metaFile.replace(/\.json$/i, '.jpg');
        let videFile = metaFile.replace(/\.json$/i, '.mp4');

        Shared.clearVideoInfo(this.videoId);

        RNFS.unlink(videFile).then(() => {

        });
        RNFS.unlink(thumFile).then(() => {
            // this.readLib();
        });
        RNFS.unlink(metaFile).then(() => {
            // this.readLib();

            Shared.isDownloaded[data.snippet.title] = false;
            Shared.videosUpdate();
            Shared.reloadLibrary();
            Actions.pop();

        }).catch((err)=>{
            Shared.isDownloaded[data.snippet.title] = false;
            Shared.videosUpdate();
            Shared.reloadLibrary();
            Actions.pop();

        });

    }

    render() {
        return (<FadeInView style={{flexDirection: 'row', padding: 20}}>
            {this.state.finishDate > 0 &&
            <Fragment><TimeAgo style={[{color: '#ccbb22'}, ...this.state.textStyles]} time={this.state.finishDate}/>
                <Text style={[{color: '#ccbb22'}, ...this.state.textStyles]}>{' '}otomatik olarak
                    silinecek!</Text></Fragment>}
            {!this.props.hideRemoveButton && <TouchableOpacity style={{marginStart: 'auto'}} onPress={() => {
                this.removeVideo(this.props.video)
            }}>
                <Icon
                    color="#ff2222"
                    name="ios-trash"
                    size={20}
                />
            </TouchableOpacity>}
        </FadeInView>)
    }

}