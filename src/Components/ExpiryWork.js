import React, {Component, Fragment} from "react";
import {Alert, Text, TouchableOpacity, View} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import Shared from "../Utilies/Shared";
import FadeInView from "react-native-fade-in-view";
import RNFS from "react-native-fs";
import moment from "moment/moment";
import TimeAgo from 'react-native-timeago';

require('moment/locale/tr');
moment.locale('tr-TR');

export default class extends Component {

    constructor(props) {
        super(props);
        this.videoId = props.video.id.videoId ? props.video.id.videoId : props.video.id;
        this.state = {
            finishDate: 0,
            textStyles: props.textStyles ? props.textStyles : {}
        };

        Shared.registerExpiryWorkUpdater(this.videoId,this._reloadVideoInfo.bind(this));
    }

    _reloadVideoInfo(){
        Shared.getVideoInfo(this.videoId, (info) => {
            this.setState(info, () => {
                const now = (new Date).valueOf();
                if (this.state.finishDate > 0 && now > this.state.finishDate) {
                    //alert(JSON.stringify(this.props.offline));
                    setTimeout(()=>{
                        let data = this.props.video;
                        data.offline = this.props.offline;
                        this.removeVideo(data, true);
                        Shared.videosUpdate();
                    },200);
                }

            });

        });
    }

    componentWillMount() {
        this._reloadVideoInfo();
    }

    removeVideo(data, forced) {

        if (!forced) {

            return Alert.alert(
                'Emin misiniz?',
                'Kütüphaneden kalıcı olarak kaldırılacak',
                [
                    {text: 'İptal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'Kaldır', onPress: () => this.removeVideo(data, true)},
                ],
                {cancelable: false}
            );

            return false;
        }


        let metaFile = data.offline;
        let thumFile = metaFile.replace(/\.json$/i, '.jpg');
        let videFile = metaFile.replace(/\.json$/i, '.mp4');

        Shared.clearVideoInfo(this.videoId);

        RNFS.unlink(videFile).then(() => {
            //this.readLib();
        });
        RNFS.unlink(thumFile).then(() => {
            // this.readLib();
        });
        RNFS.unlink(metaFile).then(() => {
            // this.readLib();

            Shared.isDownloaded[data.snippet.title] = false;
            Shared.videosUpdate();
            Shared.reloadLibrary();
        }).catch((err) => {
            Shared.isDownloaded[data.snippet.title] = false;
            Shared.videosUpdate();
            Shared.reloadLibrary();
        });

    }


    render() {
        return (<Fragment>
            {this.state.finishDate > 0 &&
            <View style={{flexDirection: 'row'}}><TimeAgo style={[{color: '#ccbb22'}, this.state.textStyles]}
                                                          time={this.state.finishDate}/>
                <Text style={[{color: '#ccbb22'}, this.state.textStyles]}>{' '}silinecek!</Text></View>}

        </Fragment>)
    }

}