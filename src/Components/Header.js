/**
 * Created by Oktay on 9/24/2017.
 */

import React, {Component} from "react";
import {StatusBar, Text, TouchableOpacity, View} from "react-native";

import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import styleGlobal from "../Styles/global";
import {Actions} from "react-native-router-flux";


export default class extends Component {


    render() {
        return (
            <View style={styleGlobal.titleBar}>
                <View style={styleGlobal.titleBarButton} />
                <View style={styleGlobal.titleBarCenter}>
                    <Text style={styleGlobal.titleBarText}>{this.props.title}</Text>
                </View>
                <View style={styleGlobal.titleBarButton}/>
            </View>
        );
    }
}