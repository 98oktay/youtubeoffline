import React,{Component} from "react";
import {View,Animated, Easing} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';

const AnimatedIcon = Animated.createAnimatedComponent(Icon);

export default class extends Component {

    constructor(props) {
        super(props)

        this.spinValue = new Animated.Value(0);

    }

    componentDidMount () {
        this.spin()
    }

    spin () {

        this.spinValue.setValue(0);

        Animated.timing(
            this.spinValue,
            {
                toValue: 1,
                duration: 400,
                easing: Easing.linear
            }
        ).start(() => this.spin())
    }

    render() {
        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        });

        return <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 100}}><AnimatedIcon
            color="#9a9a9a"
            name="ios-refresh"
            style={{
                transform: [{rotate: spin}]
            }}
            size={60}
        /></View>
    }
}