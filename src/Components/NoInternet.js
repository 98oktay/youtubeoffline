import React, {Component} from "react";
import {Image, ListView, RefreshControl, ScrollView, StatusBar, Text, TouchableOpacity, View} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import globalStyles from '../Styles/global';
import Shared from "../Utilies/Shared";
import FadeInView from "react-native-fade-in-view";

export default class extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        return (<FadeInView style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Icon
                color="#cecece"
                name="ios-wifi-outline"
                size={150}
            />
            <Text style={{color: "#cecece"}}>Çevrimdışısın :(</Text>
            <TouchableOpacity onPress={()=>{  Shared.switchMainTab(1); }} style={[globalStyles.sendBtn, {
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                padding: 10,
                marginTop: 50
            }]}>
                <Icon
                    name="ios-checkmark-circle-outline"
                    size={30}
                    color="#ffffff"
                />
                <Text style={{color: "#cecece", marginLeft: 5}}> İndirilen içeriklere gözat </Text>
            </TouchableOpacity>
        </FadeInView>)
    }

}