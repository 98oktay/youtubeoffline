import React, {Component} from "react";
import {Image, ListView, RefreshControl, ScrollView, StatusBar, Text, TouchableOpacity, View} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import globalStyles from '../Styles/global';
import Shared from "../Utilies/Shared";

export default class extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        return (<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Icon
                color="#cecece"
                name="ios-sad"
                size={150}
            />
            <Text style={{color: "#cecece"}}>Burası çok ıssız..</Text>
            <TouchableOpacity onPress={()=>{  Shared.switchMainTab(0); }} style={[globalStyles.sendBtn, {
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                padding: 10,
                marginTop: 50
            }]}>
                <Icon
                    name="ios-search"
                    size={30}
                    color="#ffffff"
                />
                <Text style={{color: "#cecece", marginLeft: 5}}> İndirilebilir içeriklere gözat </Text>
            </TouchableOpacity>
        </View>)
    }

}