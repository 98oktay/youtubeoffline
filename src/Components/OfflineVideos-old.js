/**
 * Created by Oktay on 9/24/2017.
 */

import React, {Component} from "react";
import {Image, ListView, Platform, RefreshControl, ScrollView, Text, TouchableOpacity, View, Alert} from "react-native";
import styleVideos from "../Styles/videos";
import {Actions} from "react-native-router-flux";
import RNFS from "react-native-fs";
import Shared from "../Utilies/Shared";
import moment from "moment";
import TimelineBar from "./TimlineBar";
import Icon from 'react-native-vector-icons/Ionicons';
import Levenshtein from 'fast-levenshtein';
import FadeImage from "react-native-fade-image";

//alert(Levenshtein.get('GOD OF WAR PS4 TÜRKÇE BÖLÜM 32', 'DETROIT BECOME HUMAN TÜRKÇE BÖLÜM 9', { useCollator: true}));
//alert(Levenshtein.get('TÜRKÇE BÖLÜM 32', 'TÜRKÇE BÖLÜM 119', { useCollator: true}));

export default class extends Component {

    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.path !== r2.path});
        this.state = {
            refreshing: false,
            dataSource: ds.cloneWithRows([]),
        };
        Shared.reloadLibrary = this.readLib.bind(this);

    }

    componentWillMount() {
        this.readLib();
    }

    readLib() {

        RNFS.exists(Shared.libraryPath).then((status) => {
            if (!status) {
                RNFS.mkdir(Shared.libraryPath, {
                    NSURLIsExcludedFromBackupKey: true
                });
            }

            RNFS.readDir(Shared.libraryPath)
                .then((result) => {
                    let downloads = [];
                    let rLength = result.length;
                    result.forEach((item, index) => {
                        if (item.path.match(/\.json$/gi)) {
                            item.metaInfo = false;
                            item.thumbImage = item.path.replace(/\.json$/i, '.jpg');
                            RNFS.readFile(item.path, 'utf8').then((response) => {
                                item.metaInfo = JSON.parse(response);
                                if (index >= rLength - 4) {
                                    this.setState({
                                        refreshing: false,
                                        dataSource: this.state.dataSource.cloneWithRows(downloads)
                                    });
                                }
                            });
                            downloads.push(item);
                        }
                    });

                });

        });
    }

    _onRefresh() {
        this.setState({refreshing: true, dataSource: this.state.dataSource.cloneWithRows([])});
        this.readLib();
    }

    componentWillReceiveProps(nextProps) {

    }


    removeVideo(data, forced) {

        if(!forced) {

            return Alert.alert(
                'Emin misiniz?',
                'Kütüphaneden kalıcı olarak kaldırılacak',
                [
                    {text: 'İptal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'Kaldır', onPress: () => this.removeVideo(data, true)},
                ],
                {cancelable: false}
            );

            return false;
        }


        let metaFile = data.path;
        let thumFile = data.path.replace(/\.json$/i, '.jpg');
        let videFile = data.path.replace(/\.json$/i, '.mp4');

        RNFS.unlink(videFile).then(() => {
            this.readLib();
        });
        RNFS.unlink(thumFile).then(() => {
            // this.readLib();
        });
        RNFS.unlink(metaFile).then(() => {
            // this.readLib();

            Shared.isDownloaded[data.metaInfo.snippet.title] = false;
            Shared.videosUpdate();
        });

    }


    renderItem(data) {


        return data.metaInfo ?
            <View style={styleVideos.videoItem}>
                <TouchableOpacity onPress={() => {
                    Actions.player({video: data.metaInfo, offline: data.path.replace(/\.json/g, '.mp4')})
                }} style={styleVideos.videoThumb}>
                    <FadeImage style={styleVideos.thumbImage}
                           source={{uri: 'file://' + data.thumbImage}}/>
                    <Text style={styleVideos.thumbTime}>{moment.duration(data.metaInfo.duration).format()}</Text>
                    <TimelineBar videoId={data.metaInfo.id.videoId}/>
                </TouchableOpacity>
                <View style={styleVideos.videoText}>
                    <TouchableOpacity onPress={() => {
                        Actions.player({video: data.metaInfo, offline: data.path.replace(/\.json/g, '.mp4')})
                    }}>
                        <Text style={styleVideos.videoTitle}>{data.metaInfo.snippet.title.substr(0, 30)}</Text>
                    </TouchableOpacity>
                    <View style={{alignItems: 'flex-start'}}>
                        <View style={styleVideos.videoChannelWrapper}>
                            <Text
                                style={styleVideos.videoChannel}>{data.metaInfo.snippet.channelTitle.substr(0, 30)}</Text>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => {
                        Actions.player({video: data.metaInfo, offline: data.path.replace(/\.json/g, '.mp4')})
                    }} style={{maxHeight: 32, overflow: 'hidden'}}>
                        <Text style={styleVideos.videoDesc}>{data.metaInfo.snippet.description.substr(0, 110)}</Text>
                    </TouchableOpacity>
                    <View style={{flex: 1}}/>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity style={[styleVideos.removeButton, {marginLeft: 'auto'}]} onPress={() => {
                            this.removeVideo(data)
                        }}>
                            <Icon
                                color="#ff2222"
                                name="ios-trash"
                                size={20}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View> : <View/>;
    }

    render() {
        return (
            <ScrollView style={styleVideos.scrollView}>
                {this.state.refreshing && <Text style={{textAlign: 'center', paddingTop: 200}}>Yükleniyor...</Text>}
                <ListView
                    enableEmptySections={true}
                    dataSource={this.state.dataSource}
                    ref="videosList"
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh.bind(this)}

                        />
                    }
                    renderRow={(rowData) =>
                        this.renderItem(rowData)
                    }
                />
            </ScrollView>
        );
    }
}