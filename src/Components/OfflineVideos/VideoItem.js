import React, {Component} from "react";
import {Actions} from "react-native-router-flux";
import moment from "moment/moment";
import {Text, TouchableOpacity, View, StyleSheet} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import FadeImage from "react-native-fade-image";
import TimelineBar from "../TimlineBar";
import ExpiryWork from "../ExpiryWork";
import prettyBytes from "pretty-bytes";
require('moment/locale/tr');
moment.locale('tr-TR');

export default class extends Component {

    render() {
        let data = this.props.data;
        return data.metaInfo ?
            <View style={styles.videoItem}>
                <TouchableOpacity onPress={() => {
                    Actions.player({video: data.metaInfo, offline: data.path.replace(/\.json/g, '.mp4')})
                }} style={styles.videoThumb}>
                    <FadeImage style={styles.thumbImage}
                               source={{uri: 'file://' + data.thumbImage}}/>
                    <Text style={styles.thumbTime}>{moment.duration(data.metaInfo.duration).format()}</Text>
                    <View style={styles.videoChannelWrapper}>
                            <Text
                                style={styles.videoChannel}>{data.metaInfo.snippet.channelTitle.substr(0, 30)}</Text>
                    </View>

                    <TimelineBar videoId={data.metaInfo.id.videoId? data.metaInfo.id.videoId: data.metaInfo.id}/>
                </TouchableOpacity>
                <View style={styles.videoText}>
                    <TouchableOpacity onPress={() => {
                        Actions.player({video: data.metaInfo, offline: data.path.replace(/\.json/g, '.mp4')})
                    }}>
                        <Text style={styles.videoTitle}>{data.metaInfo.snippet.title.substr(0, 30)}</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity onPress={() => {
                            Actions.player({video: data.metaInfo, offline: data.path.replace(/\.json/g, '.mp4')})
                        }} style={{maxHeight: 32, overflow: 'hidden', flex:1}}>
                            <Text style={styles.videoDesc}>{data.metaInfo.snippet.description.substr(0, 110)}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={[styles.removeButton]} onPress={() => {
                            if (this.props.onRemoveClick) this.props.onRemoveClick(data)
                        }}>
                            <Icon
                                color="#ff2222"
                                name="ios-trash"
                                size={20}
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.videoSize}>{prettyBytes(data.metaInfo.fileSize)}</Text>
                    <ExpiryWork video={data.metaInfo} offline={data.path} textStyles={{fontSize:9, marginTop:5}}/>

                </View>
            </View> : <View/>;
    }
}


let styles = StyleSheet.create({
    scrollView: {
        height: '100%',
        width: '100%',
        paddingVertical: 8
    },
    videoItem: {
        flexDirection: 'row',
        padding: 16,
        paddingVertical: 8,
        borderBottomWidth: 1,
        borderBottomColor: '#333333'
    },
    groupTitle: {
        backgroundColor: '#000',
        color: '#fff',
        padding: 3,
        paddingHorizontal: 16
    },
    videoThumb: {
        width: 150,
        height: 80
    },
    thumbImage: {
        width: '100%',
        height: '100%',
        backgroundColor: '#cdcdcd',
        resizeMode: 'cover'
    },
    thumbTime: {
        backgroundColor: '#000',
        color: '#fff',
        position: 'absolute',
        right: 0,
        bottom: 0,
        padding: 3
    },
    videoText: {
        flex: 1,
        paddingLeft: 16
    },
    videoTitle: {
        color: '#ffffff',
        fontSize: 11
    },
    videoSize: {
        marginVertical: 1,
        color: '#b4b4b4',
        fontSize: 10
    },
    videoChannel: {
        color: '#ffffff',
        fontSize: 8,
    },
    videoChannelWrapper: {
        position:'absolute',
        right:5,
        top:5,
        backgroundColor: '#080808',
        paddingHorizontal: 5,
        paddingVertical: 2,
        borderRadius: 5
    },
    videoDesc: {
        color: '#888888',
        fontSize: 9
    },
    removeButton: {
        padding: 6,
    },
    removeButtonText: {
        fontSize: 10,
        color: '#ffffff'
    },
    downloadButton: {
        marginTop: 5,
        paddingVertical: 6,
    },
    downloadButtonText: {
        fontSize: 10,
        color: '#ffffff'
    }

});