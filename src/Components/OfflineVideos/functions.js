import RNFS from "react-native-fs";
import Shared from "../../Utilies/Shared";
import stringSimilarity from "string-similarity";

export let readLibrary = (callback) => {
    RNFS.exists(Shared.libraryPath).then((status) => {
        if (!status) {
            RNFS.mkdir(Shared.libraryPath, {
                NSURLIsExcludedFromBackupKey: true
            });
        }

        RNFS.readDir(Shared.libraryPath)
            .then((result) => {
                let downloads = [];
                let rLength = result.length;
                let totalJson = 0;
                let loadedJson = 0;
                let totalSize = 0;
                let videoSizes = {};
                if (!rLength) {
                    return callback([]);
                }
                result.forEach((item, index) => {
                    totalSize += item.size;
                    let key = item.path.replace(/\.(json|mp4|jpg)$/i, '');
                    if(!videoSizes[key]) videoSizes[key] = 0;
                    videoSizes[key] += item.size;
                    if (item.path.match(/\.json$/gi)) {
                        totalJson++;

                        item.metaInfo = false;
                        item.thumbImage = item.path.replace(/\.json$/i, '.jpg');
                        downloads.push(item);

                        RNFS.readFile(item.path, 'utf8').then((response) => {
                            try {
                                item.metaInfo = JSON.parse(response);
                                item.metaInfo.fileSize = videoSizes[key];
                            } catch (error) {

                            }
                            loadedJson++;

                            if (loadedJson === totalJson) {
                                callback(downloads);
                            }
                        }).catch((err) => {
                            alert(err.message, err.code);
                        });
                    }
                });
            });
    });
};

export let toGroupItems = (items) => {

    let groups = {};

    // numeric order
    items.sort(function (a, b) {

        let title_A = a.metaInfo.snippet.title;
        let title_B = b.metaInfo.snippet.title;

        if (/\d+/.test(title_A) && /\d+/.test(title_B)) {
            let num_A = title_A.match(/(\d+)/gi);
            let num_B = title_B.match(/(\d+)/gi);

            if (num_A) num_A = parseInt(num_A.pop(), 10);
            if (num_B) num_B = parseInt(num_B.pop(), 10);

            return num_B - num_A;
        }
        return 1;
    });

    // Similarity control
    for (let i = 0; i < items.length; i++) {
        let a = items[i];
        for (let j = 0; j < items.length; j++) {
            let b = items[j];

            let title_A = a.metaInfo.snippet.title;
            let title_B = b.metaInfo.snippet.title;

            let channel_A = a.metaInfo.snippet.channelTitle;
            let channel_B = b.metaInfo.snippet.channelTitle;

            let rate = stringSimilarity.compareTwoStrings(title_A, title_B);
            if (rate > 0.5 && title_A !== title_B && !b.inGroup && channel_A === channel_B) {
                if (groups[title_A]) {
                    groups[title_A].push(b);
                    b.inGroup = title_A;
                    b.rate = rate
                } else if (groups[title_B] && !a.inGroup) {
                    groups[title_B].push(a);
                    a.inGroup = title_B;
                    a.rate = rate
                } else {
                    let groupname = title_A;
                    groups[groupname] = [a, b];
                    a.inGroup = groupname;
                    b.inGroup = groupname;
                    a.rate = 1;
                    b.rate = rate
                }
            }
        }
    }

    // Out group
    for (let k = 0; k < items.length; k++) {
        let c = items[k];
        if (!c.inGroup) {
            if (!groups['others']) {
                groups['others'] = [c]
            } else {
                groups['others'].push(c);
            }
        }
    }

    let groupsClean = [];

    Object.keys(groups).forEach((key) => {
        groupsClean.push(groups[key]);
    });

    return groupsClean;
};
