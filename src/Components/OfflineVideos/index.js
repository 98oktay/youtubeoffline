/**
 * Created by Oktay on 9/24/2017.
 */

import React, {Component, Fragment} from "react";
import {ScrollView, Text, Alert, View, Easing, Animated, ActivityIndicator} from "react-native";
import styleVideos from "../../Styles/videos";
import RNFS from "react-native-fs";
import Shared from "../../Utilies/Shared";
import VideoItem from "./VideoItem";
import Icon from 'react-native-vector-icons/Ionicons';
import {readLibrary, toGroupItems} from "./functions";
import LoadingSpiner from "../LoadingSpiner";
import NoVideos from "../NoVideos";

export default class extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
            groupItems: [],
            isEmpty: false
        };

        Shared.reloadLibrary = this.readLib.bind(this);
    }

    componentWillMount() {
        this.readLib();
    }

    readLib() {
        this.setState({
            refreshing: true,
            groupItems: [],
            isEmpty: false
        },()=>{
            readLibrary(this._onReadLib.bind(this));
        });
    }

    _onReadLib(downloads) {

        let groupItems = toGroupItems(downloads);

        this.setState({
            refreshing: false,
            groupItems,
            isEmpty: !downloads.length
        });

    }

    removeVideo(data, forced) {

        if (!forced) {

            return Alert.alert(
                'Emin misiniz?',
                'Kütüphaneden kalıcı olarak kaldırılacak',
                [
                    {text: 'İptal', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'Kaldır', onPress: () => this.removeVideo(data, true)},
                ],
                {cancelable: false}
            );

            return false;
        }


        let metaFile = data.path;
        let thumFile = data.path.replace(/\.json$/i, '.jpg');
        let videFile = data.path.replace(/\.json$/i, '.mp4');

        let videoId = data.metaInfo.id.videoId ? data.metaInfo.id.videoId : data.metaInfo.id;

        Shared.clearVideoInfo(videoId);

        RNFS.unlink(metaFile).then(() => {

            Shared.isDownloaded[data.metaInfo.snippet.title] = false;
            Shared.videosUpdate();
            this.readLib();
        });

        RNFS.unlink(videFile).then(() => {
            //
        });
        RNFS.unlink(thumFile).then(() => {
            //
        });

    }

    render() {

        return (
            <ScrollView style={[styleVideos.scrollView, {paddingBottom: 10}]}>
                {this.state.refreshing && <ActivityIndicator style={{marginTop: 80}} size="large" color="#cacaca"/>}
                {this.state.isEmpty && <NoVideos/>}
                {this.state.groupItems.map((item, index) =>
                    <Fragment key={index}>
                        {item[0].inGroup ?
                            <Text style={styleVideos.groupTitle}>{item[0].metaInfo.snippet.channelTitle}</Text> :
                            this.state.groupItems.length>1 ? <Text style={styleVideos.groupTitle}>Others</Text> :
                                <Fragment/>}

                        {item.map((item, key) => <VideoItem data={item} key={key}
                                                            onRemoveClick={this.removeVideo.bind(this)}/>)}
                    </Fragment>
                )}
            </ScrollView>
        );
    }
}