import React, {Component} from "react";
import {AppState, AsyncStorage, Text, TouchableOpacity} from "react-native";
import Shared from "../../Utilies/Shared";
import Orientation from "react-native-orientation";
import {Actions} from "react-native-router-flux";
import VideoPlayer from "react-native-video-controls";
import FadeInView from "react-native-fade-in-view";
import Icon from 'react-native-vector-icons/Ionicons';
import AnimatedHideView from "react-native-animated-hide-view";

export default class extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fullscreen: false,
            playPauseVisibility: true,
            orientation: 'PORTRAIT',
            isLastTimePos: 0,
            isPaused: true,
            videoIsLoaded: false,
            infoIsLoaded: false,
            readyForPlay: false,
            info: {},
        };

        this.videoId = props.video.id.videoId ? props.video.id.videoId : props.video.id;
        this.playerTime = 0;
        this.videoDuration = 0;
        this.videoId = props.video.id.videoId ? props.video.id.videoId : props.video.id;
        this._orientationDidChange = this._orientationDidChange.bind(this);
        this.continueVideo = this.continueVideo.bind(this);
        this._appStateChange = this._appStateChange.bind(this);

        Shared.getVideoInfo(this.videoId, this.onInfoLoaded.bind(this));

    }


    onInfoLoaded(info) {
        this.setState({
            info,
            isLastTimePos: info.isLastTimePos,
            infoIsLoaded: true
        }, () => {
            if (this.state.videoIsLoaded) {
                this.onReadyForPlay();
            }
        });
    }

    onVideoLoad(info) {
        this.setVideoDuration(info.duration);
        this.setState({videoIsLoaded: true}, () => {
            if (this.state.infoIsLoaded) {
                this.onReadyForPlay();
            }

        });
    }

    onReadyForPlay() {
        this.setState({readyForPlay: true}, () => {
            setTimeout(this.continueVideo, 500);
        });
    }

    componentDidMount() {
        Orientation.addOrientationListener(this._orientationDidChange);

        AppState.addEventListener('change', this._appStateChange);

    }

    componentWillUnmount() {
        Orientation.removeOrientationListener(this._orientationDidChange);
        AppState.removeEventListener('change', this._appStateChange);

        let videoDuration = Math.floor(this.videoDuration);

        let isLastTimePos = Math.min(Math.floor(this.playerTime), videoDuration);

        const videoInfo = {...this.state.info, isLastTimePos, videoDuration};
        Shared.getVideoInfo(this.videoId, (info) => {
            videoInfo.finishDate = info.finishDate;

            if (videoDuration > 10 && isLastTimePos > 10) {

                if (!this.state.info.finishDate && (isLastTimePos / videoDuration) > 0.90) {
                    videoInfo.finishDate = (new Date).valueOf();
                    if (videoDuration > 60 * 60) { // 1 saatten uzun
                        videoInfo.finishDate += 1000 * 60 * 60 * 24 * 30;
                    }
                    else if (videoDuration > 60 * 20) { // 20 dk dan uzun
                        videoInfo.finishDate += 1000 * 60 * 60 * 24 * 14;
                    } else {

                        videoInfo.finishDate += 1000 * 60 * 60 * 24 * 7;
                    }
                }

                Shared.setVideoInfo(this.videoId, videoInfo, () => {
                    Shared.callDurationBarUpdater(this.videoId);
                    Shared.callExpiryWorkUpdater(this.videoId);
                });
            }

        });


    }

    _appStateChange(state) {
        if (state === 'active') {
            console.log('state active');
        }

        if (state === 'background') {
            if (this.videoPlayer && this.videoPlayer.player) {

                if (!this.state.isPaused) {
                    this.videoPlayer.methods.togglePlayPause();
                }
            }
        }
    }

    _orientationDidChange(orientation) {

        if (orientation !== this.state.orientation) {
            this.setState({orientation});
            if (!this.state.fullscreen && orientation === "LANDSCAPE") {
                this.videoPlayer.methods.toggleFullscreen();
            }
            if (this.state.fullscreen && orientation !== "LANDSCAPE") {
                this.videoPlayer.methods.toggleFullscreen();
            }
        }
    }

    setPlayerTime(time) {
        this.playerTime = time;
    }

    setVideoDuration(duration) {
        this.videoDuration = duration;
    }

    onFullscreenChange(state) {
        this.props.onFullscreenChange && this.props.onFullscreenChange(state)
    }

    continueVideo() {

        if (this.state.isLastTimePos) {

            this.videoPlayer.player.ref.seek(this.state.isLastTimePos);

            if (this.state.isPaused) {
                this.videoPlayer.methods.togglePlayPause();
            }

        }
    }

    playPauseVisibility(always) {
        if (this.playPauseVisibilityTmo) clearTimeout(this.playPauseVisibilityTmo);

        this.setState({playPauseVisibility: true});

        if (!always) {
            this.playPauseVisibilityTmo = setTimeout(() => {
                this.setState({playPauseVisibility: false});
            }, 2000);

        }
    }

    render() {

        return <FadeInView style={{flex: 1, position: 'relative'}}>
            <VideoPlayer
                ref={(ref) => {
                    this.videoPlayer = ref;
                }}

                seekColor="#ff2222"
                paused={this.state.isPaused}
                disablePlayPause={true}
                disableVolume={true}
                disableFullscreen={this.state.orientation === 'LANDSCAPE'}
                ignoreSilentSwitch="ignore"
                onLoad={(info) => {
                    this.onVideoLoad(info);
                }}
                onToggleControls={(state) => {
                    this.playPauseVisibility(this.state.isPaused && state);
                }}
                onPause={() => {
                    this.setState({isPaused: true});
                    this.playPauseVisibility(true);
                }}
                onPlay={() => {
                    this.setState({isPaused: false});
                    this.playPauseVisibility(false);
                }}
                onEnterFullscreen={() => {
                    this.onFullscreenChange(true)
                }}
                onExitFullscreen={() => {
                    this.onFullscreenChange(false);
                    this.playPauseVisibility(false);
                }}
                onEnd={() => {
                    this.setState({isPaused: true});
                    this.onFullscreenChange(false);
                    this.playPauseVisibility(true);
                }}
                onProgress={({currentTime}) => {
                    this.setPlayerTime(currentTime);
                }}
                onBack={() => {
                    Actions.pop();
                }}
                resizeMode='contain'
                toggleResizeModeOnFullscreen={false}

                source={{uri: 'file://' + encodeURI(this.props.video.offline)}}
                style={{
                    flex: 1,
                    width: '100%',
                    height: 160,
                    backgroundColor: '#000000'
                }}
            />
            <AnimatedHideView visible={this.state.playPauseVisibility} style={{
                position: 'absolute',
                left: '50%', top: '50%',
                marginLeft: -10,
                marginTop: -25
            }}>
                <TouchableOpacity onPress={() => {
                    this.videoPlayer.methods.togglePlayPause();
                }}>
                    <Icon
                        name={this.state.isPaused ? 'ios-play' : 'ios-pause'}
                        size={50}
                        color="#ffffff"
                    />
                </TouchableOpacity></AnimatedHideView>

        </FadeInView>;
    }
}