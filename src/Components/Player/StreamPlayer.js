import React, {Component} from "react";
import {WebView} from "react-native";

export default class extends Component {

    constructor(props) {
        super(props);
        this.state = {};

        this.videoId = props.video.id.videoId ? props.video.id.videoId : props.video.id;
    }

    onNavigationStateChange(navState) {
        if (navState.url.indexOf('youtube.com') !== -1) {
            return false;
        }
    }

    render() {

        return <WebView
            style={{flex: 1, width: '100%', height: 160, backgroundColor: '#000000'}}
            onNavigationStateChange={this.onNavigationStateChange}
            javaScriptEnabled={true}
            source={{uri: `https://www.youtube.com/embed/${this.videoId}?rel=0&autoplay=1&showinfo=0&controls=1`}}
        />;
    }
}