import React, {Component} from "react";
import StreamPlayer from "./StreamPlayer";
import LocalPlayer from "./LocalPlayer";

export default class extends Component {

    render() {

        return this.props.video.offline ? <LocalPlayer onFullscreenChange={this.props.onFullscreenChange} video={this.props.video}/> :
            <StreamPlayer video={this.props.video}/>;
    }
}