import React, {Component, Fragment} from "react";
import {StyleSheet, View} from "react-native";
import Shared from "../Utilies/Shared";
import AnimatedHideView from "react-native-animated-hide-view";

export default class extends Component {

    constructor(props) {
        super(props);
        this.items = {};

        this.state = {
            progressValue: 0
        };

        Shared.onProgressUpdate = this.setProgress.bind(this);
        Shared.onProgressFinish = this.finishProgress.bind(this);

    }

    finishProgress(jobId) {
        if (this.items[jobId]) {
            delete this.items[jobId];
        }
        this.calcProgress();
    }

    calcProgress() {

        const {items} = this;
        const keys = Object.keys(items);
        if (!keys.length) {
            return this.setState({
                progressValue: 0
            })
        }
        let calcedRate = 0;
        keys.forEach((key) => {
            calcedRate += items[key].bytesWritten / items[key].contentLength;
        });

        this.setState({
            progressValue: calcedRate / keys.length
        })
    }

    setProgress(progress, jobId) {

        this.items[jobId] = progress;
        // todo timeout finish

        this.calcProgress();
    }

    render() {
        return (<Fragment>
            <AnimatedHideView duration={1000} visible={this.state.progressValue > 0} style={styles.progressWrapper}>
                <View style={[styles.progressBar, {width: `${this.state.progressValue * 100}%`}]}/>
            </AnimatedHideView>
        </Fragment>)
    }
}

const styles = StyleSheet.create({
    progressWrapper: {
        position: 'absolute',
        top: -5,
        left: 0,
        right: 0,
        width: '100%',
        height: 5,
        backgroundColor: '#30abfd'
    },
    progressBar: {
        width: 0,
        height: 5,
        backgroundColor: '#ffffff'
    }

});