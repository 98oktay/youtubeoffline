import React, {Component} from "react";
import {View} from "react-native";
import Shared from "../Utilies/Shared";

export default class extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLastTimePos: 0,
            videoDuration: 0
        };

        this.reloadState = this.reloadState.bind(this);

        Shared.registerDurationBarUpdater(this.props.videoId, this.reloadState)
    }

    componentDidMount() {

        this.reloadState();
    }

    reloadState() {

        Shared.getVideoInfo(this.props.videoId, (videoInfo)=>{

            if (videoInfo) {

                this.setState({
                    isLastTimePos: videoInfo.isLastTimePos,
                    videoDuration: videoInfo.videoDuration
                });
            }
        });

    }

    render() {
        return (<View style={{height: 3, marginTop:-2, width: '100%', backgroundColor: '#454545'}}>
            {this.state.videoDuration > 0 && <View style={{
                height: 3,
                width: ((this.state.isLastTimePos / this.state.videoDuration * 100) + '%'),
                backgroundColor: '#ff0000'
            }}/>}
        </View>)
    }

}