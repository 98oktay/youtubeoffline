/**
 * Created by Oktay on 9/24/2017.
 */

import React, {Component} from "react";
import {
    ActivityIndicator, Image, ListView, RefreshControl, ScrollView, StatusBar, Text, TouchableOpacity,
    View
} from "react-native";
import styleVideos from "../Styles/videos";
import {Actions} from "react-native-router-flux";
import DownloadButton from "./DownloadButton";
import Shared from "../Utilies/Shared";
import moment from "moment";
import momentDurationFormatSetup from "moment-duration-format";
import Search from "react-native-search-box";
import FadeImage from "react-native-fade-image";
import LoadingSpiner from "./LoadingSpiner";


const API = 'AIzaSyAOYG1Ai4mZy6L-ifZgQ8bzS87vA6v3JdA';
const result = 25;
const searchEndpoint = "https://www.googleapis.com/youtube/v3/search";
const videosEndpoint = "https://www.googleapis.com/youtube/v3/videos";
let firstListingQuery = "";
let finalURL = ``;

export default class extends Component {

    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.etag !== r2.etag || r1.duration !== r2.duration});
        this.state = {
            refreshing: true,
            dataSource: ds.cloneWithRows([]),
            searchQuery: firstListingQuery,
            lastItems: [],
            contentDetails: {}
        };

        this.loadData(true);

        Shared.videosUpdate = () => {
            this.setState({
                refresing: false,
                dataSource: this.state.dataSource.cloneWithRows([])
            });
            this.setState({
                refresing: false,
                dataSource: this.state.dataSource.cloneWithRows(this.state.lastItems)
            });

        };

        Shared.videosUpdate = Shared.videosUpdate.bind(this);

    }


    loadData(isTrends) {

        if (isTrends) {
            finalURL = `${videosEndpoint}?key=${API}&chart=mostPopular&part=snippet,id&regionCode=TR&relevanceLanguage=TR-tr&type=video&order=date&maxResults=${result}`;
        } else {
            finalURL = `${searchEndpoint}?key=${API}&q=${this.state.searchQuery}&part=snippet,id&regionCode=TR&relevanceLanguage=TR-tr&type=video&order=date&maxResults=${result}`;
        }

        this.setState({
            refresing: true,
        });

        fetch(finalURL)
            .then((response) => response.json())
            .then((responseJson) => {
                let ids = [];

                responseJson.items.forEach(item => {
                    const id = typeof item.id === "string" ? item.id : item.id.videoId;
                    ids.push(id);
                });

                this.setState({
                    lastItems: responseJson.items,
                    //dataSource: this.state.dataSource.cloneWithRows(responseJson.items)
                }, () => {
                    this.loadVideoDurations(ids);
                });
            });

    }

    loadVideoDurations(ids) {

        let durationUrl = `${videosEndpoint}?part=snippet%2CcontentDetails%2Cstatistics%2Cstatus&id=${ids.join(',')}&key=${API}`;


        fetch(durationUrl)
            .then((response) => response.json())
            .then((responseJson) => {

                let contentDetails = {};

                responseJson.items.forEach(item => {
                    contentDetails[item.id] = item.contentDetails;

                });

                this.state.lastItems.forEach(item => {
                    const id = typeof item.id === "string" ? item.id : item.id.videoId;
                    if (contentDetails[id]) {

                        item.duration = contentDetails[id].duration;
                    }
                });

                this.setState({
                    contentDetails,
                    refreshing: false,
                    lastItems: this.state.lastItems,
                    dataSource: this.state.dataSource.cloneWithRows(this.state.lastItems)
                });
            });
    }

    _onRefresh() {
        this.setState({refreshing: true, dataSource: this.state.dataSource.cloneWithRows([])});
        this.loadData();
    }

    componentWillReceiveProps(nextProps) {


    }

    render() {
        return (
            <View>
                <Search
                    placeholder = "Arama"
                    ref="search_box"
                    value={this.state.searchQuery}
                    backgroundColor="#222222"
                    cancelTitle="İptal"
                    onSearch={(value) => {
                        this.setState({
                            refreshing: true,
                            dataSource: this.state.dataSource.cloneWithRows([]),
                            searchQuery: value
                        }, this.loadData)
                    }}
                />
                <ScrollView style={styleVideos.scrollView}>
                    {this.state.refreshing && <ActivityIndicator style={{marginTop: 80}} size="large" color="#cacaca"/>}
                    <ListView
                        enableEmptySections={true}
                        dataSource={this.state.dataSource}
                        ref="videosList"
                        style={{paddingBottom: 10}}
                        renderRow={(rowData) =>
                            <View style={styleVideos.videoItem}>
                                <TouchableOpacity onPress={() => {
                                    Actions.player({video: rowData})
                                }} style={styleVideos.videoThumb}>
                                    <FadeImage style={styleVideos.thumbImage}
                                               source={{uri: rowData.snippet.thumbnails.high.url}}/>
                                    <Text
                                        style={styleVideos.thumbTime}>{moment.duration(rowData.duration).format()}</Text>
                                    <View style={styleVideos.videoChannelWrapper}>
                                        <Text
                                            style={styleVideos.videoChannel}>{rowData.snippet.channelTitle.substr(0, 30)}</Text>
                                    </View>                                        
                                </TouchableOpacity>
                                <View style={styleVideos.videoTextButtonWrapper}>
                                    <View style={styleVideos.videoText}>
                                        <TouchableOpacity onPress={() => {
                                            Actions.player({video: rowData})
                                        }}>
                                            <Text
                                                style={styleVideos.videoTitle}>{rowData.snippet.title.substr(0, 30)}</Text>
                                        </TouchableOpacity>
                                        
                                        <TouchableOpacity onPress={() => {
                                            Actions.player({video: rowData})
                                        }} style={{maxHeight: 32, overflow: 'hidden'}}>
                                            <Text
                                                style={styleVideos.videoDesc}>{rowData.snippet.description.substr(0, 110)}</Text>
                                        </TouchableOpacity>

                                    </View>
                                    <DownloadButton info={rowData}/>

                                </View>
                            </View>
                        }
                    />
                </ScrollView>
            </View>
        );
    }
}