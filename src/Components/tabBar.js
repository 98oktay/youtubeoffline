/**
 * Created by Oktay on 9/21/2017.
 */

import React, {Component} from 'react';

import {
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Shared from "../Utilies/Shared";
import ProgressBar from "./ProgressBar";

export default class TabBar extends Component {


    constructor(props) {
        super(props);
        this.tabIcons = [];
        this.tabTitles = [];
        this.tabTitleTexts = ['Videos', 'Offline'];

        Shared.switchMainTab = this.props.goToPage.bind(this);

    }


    componentDidMount() {

        this._listener = this.props.scrollValue.addListener(this.setAnimationValue.bind(this));
    }

    setAnimationValue({value}) {

        this.tabIcons.forEach((icon, i) => {
            const progress = Math.min(1, Math.abs(value - i));
            icon.setNativeProps({
                style: {
                    color: this.iconColor(progress),
                    transform: [{
                        scale: 1 - (progress / 4),
                    }]
                },
            });
        });

    }

    //color between rgb(59,89,152) and rgb(204,204,204)
    iconColor(progress) {
        const red = 255 + (204 - 255) * progress;
        const green = 0 + (204 - 0) * progress;
        const blue = 0 + (204 - 0) * progress;
        return `rgb(${red}, ${green}, ${blue})`;
    }

    render() {
        return <View style={[styles.tabs, this.props.style]}>
            <ProgressBar/>
            <View style={{flexDirection: 'row', width: '100%', height: '100%'}}>
                {this.props.tabs.map((tab, i) => {
                    return <TouchableOpacity activeOpacity={0.9} key={tab} onPress={() => {
                        this.props.goToPage(i)
                    }} style={styles.tab}>
                        <Icon
                            name={tab}
                            size={30}
                            style={{transform: [{scale: this.props.activeTab === i ? 1 : 0.75}]}}
                            color={this.props.activeTab === i ? 'rgb(255,0,0)' : 'rgb(204,204,204)'}
                            ref={(icon) => {
                                this.tabIcons[i] = icon;
                            }}
                        />
                        <Text style={styles.tabTitleText}>{this.tabTitleTexts[i]}</Text>
                    </TouchableOpacity>;
                })}
            </View>
        </View>;
    }
}

const styles = StyleSheet.create({

    tabTitleText: {
        color: '#ffffff',
        fontSize: 11,
        lineHeight: 11
    },
    tab: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: '#333333'
    },
    tabs: {
        position: 'relative',
        height: 50,
        flexDirection: 'column',
        paddingTop: 0,
    },
});