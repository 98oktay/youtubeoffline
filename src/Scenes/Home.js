/**
 * Created by Oktay on 9/21/2017.
 */

import React, {Component} from 'react';
import {
    Text,
    View,
    NetInfo,
    ScrollView,
    Image,
    StatusBar
} from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import styleGlobal from '../Styles/global';
import TabBar from "../Components/tabBar";
import OfflineVideos from "../Components/OfflineVideos";
import Videos from "../Components/Videos";
import NoInternet from "../Components/NoInternet";
import RNFS from "react-native-fs";
import Shared from "../Utilies/Shared";
import Images from "../Images";
import FadeImage from "react-native-fade-image";

export default class OfflaynTv extends Component {

    constructor(props) {
        super(props);

        this.state = {
            connectionType: "none",
        };

        RNFS.exists(Shared.libraryPath).then((status) => {
            if (!status) {
                RNFS.mkdir(Shared.libraryPath, {
                    NSURLIsExcludedFromBackupKey: true
                });
            }
        });

        this.handleConnectivityChange = this.handleConnectivityChange.bind(this);


        NetInfo.getConnectionInfo().then((connectionInfo) => {
            Shared.connectionType = connectionInfo.type;
            this.setState({connectionType: Shared.connectionType})
        });

        NetInfo.addEventListener(
            'connectionChange',
            this.handleConnectivityChange
        );

    }


    handleConnectivityChange(connectionInfo) {
        if (connectionInfo) {

            Shared.connectionType = connectionInfo.type;
            this.setState({connectionType: Shared.connectionType})

        }
    }

    render() {
        return <View style={{flex: 1, backgroundColor:'#222222'}}>
            <View style={styleGlobal.homeHeader}>
                <FadeImage style={{height: 80, width: 600, marginTop: 32}} source={Images.logo}/>
                <StatusBar
                    animated={true}
                    showHideTransition="slide"
                    hidden={false}
                    translucent={true}
                    backgroundColor="transparent"
                    barStyle="light-content"
                />
            </View>
            <ScrollableTabView
                scrollWithoutAnimation={true}
                initialPage={0}
                renderTabBar={() => <TabBar/>}
                tabBarPosition={"bottom"}
            >
                <View tabLabel="ios-podium" style={[styleGlobal.tabView, {paddingBottom: 40}]}>
                    {this.state.connectionType === "none" ? <NoInternet/> : <Videos/>}

                </View>
                <ScrollView tabLabel="ios-bookmarks" style={styleGlobal.tabView}>
                    <OfflineVideos/>
                </ScrollView>
            </ScrollableTabView>
        </View>;
    }
};
