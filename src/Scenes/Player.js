/**
 * Created by Oktay on 9/24/2017.
 */

import React, {Component} from "react";
import {ScrollView, StatusBar, Text, TouchableOpacity, View, WebView, AsyncStorage, AppState} from "react-native";

import Ionicons from "react-native-vector-icons/dist/Ionicons";
import styleGlobal from "../Styles/global";
import {Actions} from "react-native-router-flux";
import VideoPlayer from 'react-native-video-controls';
import Orientation from 'react-native-orientation'
import Shared from "../Utilies/Shared";


export default class extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fullscreen: false,
            orientation: 'PORTRAIT',
            isLastTimePos: 0,
            isPaused: true,
            videoIsLoaded: false,
            infoIsLoaded: false,
            info: [],

        };

        this.playerTime = 0;
        this.videoDuration = 0;
        this.videoId = props.video.id.videoId ? props.video.id.videoId : props.video.id;
        this._orientationDidChange = this._orientationDidChange.bind(this);
        this.continueVideo = this.continueVideo.bind(this);
        this._appStateChange = this._appStateChange.bind(this);

        Shared.getVideoInfo(this.videoId, this.onInfoLoaded.bind(this));
    }

    onInfoLoaded(info) {
        this.setState({
            info,
            isLastTimePos: info.isLastTimePos,
            infoIsLoaded: true
        });

        if (this.state.videoIsLoaded) {
            this.onReadyForPlay();
        }
    }

    onVideoLoad() {
        this.setState({videoIsLoaded: true});
        if (this.state.infoIsLoaded) {
            this.onReadyForPlay();
        }
    }

    onReadyForPlay() {

        setTimeout(this.continueVideo, 500);
    }

    componentDidMount() {
        Orientation.addOrientationListener(this._orientationDidChange);

        AppState.addEventListener('change', this._appStateChange);

        AsyncStorage.getItem(this.props.video.id.videoId + "videoInfo").then((data) => {
            if (data) {
                const videoInfo = JSON.parse(data);
                this.setState({
                    isLastTimePos: videoInfo.isLastTimePos
                }, () => {

                    setTimeout(this.continueVideo, 500);

                })
            }
        });
    }

    componentWillUnmount() {
        Orientation.removeOrientationListener(this._orientationDidChange);
        AppState.removeEventListener('change', this._appStateChange);

        let videoDuration = Math.floor(this.videoDuration);

        let isLastTimePos = Math.min(Math.floor(this.playerTime), videoDuration);

        const videoInfo = {isLastTimePos, videoDuration};

        if (videoDuration > 10 && isLastTimePos > 10) {
            AsyncStorage.setItem(this.props.video.id.videoId + "videoInfo", JSON.stringify(videoInfo), () => {
                    Shared.callDurationBarUpdater(this.props.video.id.videoId);
                }
            );
        }

    }

    _appStateChange(state) {
        if (state === 'active') {
            console.log('state active');
        }

        if (state === 'background') {
            if (this.videoPlayer && this.videoPlayer.player) {

                this.setState({isPaused: false});
            }
        }
    }

    _orientationDidChange(orientation) {

        delete this.state.isPaused;
        (orientation !== this.state.orientation) && this.setState({orientation})
    }

    setPlayerTime(time) {
        this.playerTime = time;
    }

    setVideoDuration(duration) {
        this.videoDuration = duration;
    }

    continueVideo() {

        if (this.state.isLastTimePos) {

            this.videoPlayer.player.ref.seek(this.state.isLastTimePos);
            this.setState({paused: false})
        }
    }


    onNavigationStateChange = navState => {
        if (navState.url.indexOf('youtube.com') === 0) {
            return false;
        }
    };

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#000000'}}>
                <StatusBar
                    animated={true}
                    showHideTransition="slide"
                    hidden={true}
                    translucent={true}
                    backgroundColor="transparent"
                    barStyle="light-content"
                />
                <View style={[styleGlobal.logoWrapepr, {backgroundColor: '#f00'}]}>

                    <TouchableOpacity onPress={() => {
                        Actions.pop()
                    }} style={[styleGlobal.titleBarButton, {position: 'absolute', right: 10, top: 25}]}>
                        <Ionicons name="ios-close" size={50} color="white"/>
                    </TouchableOpacity>
                </View>
                {this.props.offline ?
                    <VideoPlayer

                        ref={(ref) => {
                            this.videoPlayer = ref;
                        }}
                        paused={this.state.isPaused}
                        playInBackground={true}
                        playWhenInactive={true}
                        ignoreSilentSwitch="ignore"
                        onLoad={() => {
                            this.onVideoLoad();
                        }}
                        onEnterFullscreen={() => {
                            this.setState({fullscreen: true})
                        }}
                        onExitFullscreen={() => {
                            this.setState({fullscreen: false})
                        }}
                        onEnd={() => {
                            this.setState({fullscreen: false})
                        }}
                        onProgress={({currentTime, seekableDuration}) => {
                            this.setPlayerTime(currentTime);
                            this.setVideoDuration(seekableDuration);
                        }}
                        onBack={() => {
                            Actions.pop();
                        }}
                        resizeMode='contain'
                        toggleResizeModeOnFullscreen={false}

                        source={{uri: 'file://' + encodeURI(this.props.offline)}}
                        style={{flex: 1, width: '100%', height: 160, backgroundColor: '#000000'}}
                    /> :
                    <WebView
                        style={{flex: 1, width: '100%', height: 160, backgroundColor: '#000000'}}
                        onNavigationStateChange={this.onNavigationStateChange}
                        javaScriptEnabled={true}
                        source={{uri: `https://www.youtube.com/embed/${this.videoId}?rel=0&autoplay=1&showinfo=0&controls=1`}}
                    />
                }
                {!(this.state.fullscreen || this.state.orientation === 'LANDSCAPE') && <View style={{flex: 1.6}}>
                    <ScrollView style={{flex: 1, padding: 16}}>
                        <Text style={{
                            fontSize: 16,
                            marginBottom: 16,
                            color: '#aaa'
                        }}>{this.props.video.snippet.title}</Text>
                        <Text style={{color: '#777'}}>{this.props.video.snippet.description}</Text>
                    </ScrollView>
                </View>
                }


            </View>
        );
    }
}

