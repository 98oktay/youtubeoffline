/**
 * Created by Oktay on 9/24/2017.
 */

import React, {Component} from "react";
import {ScrollView, StatusBar, Text, View } from "react-native";

import Orientation from 'react-native-orientation'
import Player from "../Components/Player";
import FadeInView from "react-native-fade-in-view";
import ExpiryOptions from "../Components/ExpiryOptions";

export default class extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orientation: '',
            fullscreen: false
        };
        this.video = {...this.props.video, offline: this.props.offline}
        this._orientationDidChange = this._orientationDidChange.bind(this);

    }

    componentDidMount() {
        Orientation.addOrientationListener(this._orientationDidChange);

    }

    componentWillUnmount() {
        Orientation.removeOrientationListener(this._orientationDidChange);
    }

    _orientationDidChange(orientation) {

        (orientation !== this.state.orientation) && this.setState({orientation})

    }

    onFullscreenChange(state) {
        this.setState({fullscreen: state})
    }

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#000000'}}>
                <StatusBar
                    animated={true}
                    showHideTransition="slide"
                    hidden={true}
                    translucent={true}
                    backgroundColor="transparent"
                    barStyle="light-content"
                />
                <Player video={this.video} onFullscreenChange={this.onFullscreenChange.bind(this)}/>
                {this.state.orientation !== 'LANDSCAPE' && !this.state.fullscreen && <FadeInView style={{flex: 1.6}}>
                    <ScrollView style={{flex: 1, padding: 16}}>
                        <Text style={{
                            fontSize: 16,
                            marginBottom: 16,
                            color: '#aaa'
                        }}>{this.props.video.snippet.title}</Text>
                        <Text style={{color: '#777'}}>{this.props.video.snippet.description}</Text>
                    </ScrollView>
                    <ExpiryOptions video={this.video} />
                </FadeInView>}
            </View>
        );
    }
}

