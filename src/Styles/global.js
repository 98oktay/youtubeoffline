/**
 * Created by Oktay on 9/23/2017.
 */
import {StyleSheet} from "react-native";


let shadowProps = {
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
};

export default StyleSheet.create({
    homeHeader: {
        backgroundColor: '#333333',
        paddingTop:20,
        height:70,
        justifyContent:'center',
        alignItems:'center'
    },
    homeHeaderText: {
        color: "red",
fontWeight:"bold",
        fontSize: 22
    },
    homeWrapper: {
        backgroundColor: '#333333',
    },
    logoWrapepr: {
        width: '100%',
        height: 0,
    },
    logo: {
        height: 60,
        width: '50%',
        resizeMode: 'contain',
        alignSelf: 'flex-start',
        marginTop: 20,
    },
    titleBar: {
        backgroundColor: '#ff0000',
        justifyContent: 'center',
        paddingTop: 20,
        flexDirection: 'row',
        width: '100%',
        height: 80,
        elevation: 5,
        zIndex: 2,
        ...shadowProps
    },
    titleBarButton: {
        width: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleBarCenter: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    titleBarText: {
        textAlign: 'center',
        fontSize: 18,
        color: '#ffffff'
    },

    categoryButton: {
        width: '100%',
        marginBottom: -30
    },
    categoryImage: {
        height: 140,
        width: '90%',
        resizeMode: 'contain'
    },
    categoryImageRight: {
        alignSelf: 'flex-end',
    },
    listContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        backgroundColor: '#dedede',
    },
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#eeeeee',
        resizeMode: 'cover'
    },
    applicationButton: {
        width: '100%',
        backgroundColor: '#ff0000',
        zIndex: 1,
        ...shadowProps
    },
    applicationButtonText: {
        fontSize: 16,
        textAlign: 'center',
        paddingVertical: 16,
        color: '#ffffff'
    },
    commingText: {
        fontSize: 40,
        textAlign: 'center',
        paddingVertical: 16,
        color: '#787878'
    },
    answButtonGreen: {
        backgroundColor: '#66bb88',
        padding: 50,
        paddingVertical: 10,
    },
    answButton: {
        backgroundColor: '#ee4444',
        padding: 28,
        paddingVertical: 10,
        margin: 5
    },
    formTitle: {
        fontSize: 30,
        width: '100%',
        textAlign: 'left'
    },
    textLabel: {
        width: '100%',
        textAlign: 'left',
        marginTop: 20,
        marginBottom: 10
    },

    textInput: {
        width: '100%',
        marginVertical: 20,
        borderWidth: 2,
        borderColor: '#aaa',
        height: 44,
        padding: 8
    },
    sendBtn: {
        backgroundColor: '#3a68b5',
        padding: 32,
        paddingVertical: 10,
        margin: 5,
        ...shadowProps
    },
    sendBtnText: {
        fontSize: 16,
        color: '#fff',
        textAlign: 'center'
    },
    tabView: {
        flex: 1,
        padding: 0,
        backgroundColor: '#222222',
    },
});