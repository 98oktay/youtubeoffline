/**
 * Created by Oktay on 9/23/2017.
 */
import {StyleSheet} from "react-native";


let shadowProps = {
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
};

export default StyleSheet.create({
    scrollView: {
        height: '100%',
        width: '100%',
        paddingVertical: 8
    },
    videoItem: {
        flexDirection: 'row',
        padding: 16,
        paddingVertical: 8,
        borderBottomWidth: 1,
        borderBottomColor: '#333333'
    },
    groupTitle: {
        backgroundColor: '#000',
        color: '#fff',
        padding: 3,
        paddingHorizontal: 16
    },
    videoThumb: {
        width: 150,
        height: 80
    },
    thumbImage: {
        width: '100%',
        height: '100%',
        backgroundColor: '#cdcdcd',
        resizeMode: 'cover'
    },
    thumbTime: {
        backgroundColor: '#000',
        color: '#fff',
        position: 'absolute',
        right: 0,
        bottom: 0,
        padding: 3
    },
    videoText: {
        flex: 1,
        paddingLeft: 16
    },
    videoTitle: {
        color: '#ffffff',
        fontSize: 12
    },
    videoChannel: {
        color: '#ffffff',
        fontSize: 8,
    },
    videoTextButtonWrapper: {        
        flexDirection: 'row',
        flex: 1,
    },
    videoChannelWrapper: {
        position:'absolute',
        right:5,
        top:5,
        backgroundColor: '#080808',
        paddingHorizontal: 5,
        paddingVertical: 2,
        borderRadius: 5
    },
    videoDesc: {
        color: '#888888',
        fontSize: 9
    },
    removeButton: {
        marginTop: 5,
        paddingVertical: 6,
    },
    removeButtonText: {
        fontSize: 10,
        color: '#ffffff'
    },
    downloadWrapper: {
        alignSelf:'center'
    },
    downloadButton: {
        backgroundColor:'red',
        marginTop: 5,
        paddingVertical: 6,
    },
    downloadButtonText: {
        fontSize: 10,
        color: '#ffffff'
    }

});