import slugify from "slugify";

let foundService = 'https://catchvideo.net/getvideo';
let foundServicePickvideo = 'https://pickvideo.net/download';
let foundServiceYoodownload = 'https://yoodownload.com/download.php';
let foundServiceY2mate = 'https://y2mate.com/analyze/ajax';

import RNFS from "react-native-fs";
import {Platform, Alert} from "react-native";
import Shared from "./Shared";

export default {

    offlineForce(item, onError, onStart, onProgress, onFinish, trayAgain = true) {

        let id = typeof item.id === "string" ? item.id : item.id.videoId;
        let toFormData = function (data) {
            return Object.keys(data).map(function (keyName) {
                return encodeURIComponent(keyName) + '=' + encodeURIComponent(data[keyName])
            }).join('&');
        };

        fetch(foundServiceY2mate, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-Requested-With': 'XMLHttpRequest',
                referer: 'https://yoodownload.com/'
            },
            body: toFormData({
                url: `https://www.youtube.com/watch?v=${id}`,
                ajax: 1
            }),
        })
            .then((response) => {
                return response.json();
            })
            .then((body) => {
                let result = body.result;
                let downloadurl = result.match(/(https\:\/\/.*videoplayback[^"']*)/mi);
                let videoTitle = result.match(/"([^"]*)\.mp4/mi).pop();

                let data = {
                    output: {
                        title: videoTitle,
                        format: [
                            {
                                url: downloadurl[0],
                                qres_disp: "360p",
                                f_output: "mp4"
                            },
                            {
                                url: downloadurl[1],
                                qres_disp: "240p",
                                f_output: "mp4"
                            }
                        ]
                    }
                };

                this.onFetchInfo(data, item, onError, onStart, onProgress, onFinish, trayAgain);


            }).catch((error) => {
            if (onError) onError({
                code: 'SERVICE_RESPONSE',
                error
            });
        });
    },

    qualitySelecttor(data, item, onError, onStart, onProgress, onFinish) {

        const formatButtons = [];
        const formats = data.output.format;

        formats.map((format) => {
            if (format.f_output === "mp4" && format.type_stream !== "VIDEOONLY" && format.type_stream !== "AUDIOONLY") {
                formatButtons.push({
                    text: format.qres_disp,
                    onPress: () => {
                        data.output.format = [format];
                        this.onFetchInfo(data, item, onError, onStart, onProgress, onFinish);
                    }
                });
            }

        });


        Alert.alert(
            'Kalite Seçin',
            '',
            [
                ...formatButtons,
                {
                    text: 'Cancel', onPress: () => {
                        onError();
                    }, style: 'cancel'
                },
            ],
            {cancelable: true}
        );

    },

    offline(item, onError, onStart, onProgress, onFinish) {

        let toFormData = function (data) {
            return Object.keys(data).map(function (keyName) {
                return encodeURIComponent(keyName) + '=' + encodeURIComponent(data[keyName])
            }).join('&');
        };
        let id = typeof item.id === "string" ? item.id : item.id.videoId;

        fetch(foundService, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-Requested-With': 'XMLHttpRequest'
            },
            body: toFormData({
                url: `https://www.youtube.com/watch?v=${id}`, ap: 'false', status: 'start'
            }),
        })
            .then((response) => response.json())
            .then((data) => {

                this.onFetchInfo(data, item, onError, onStart, onProgress, onFinish);

            }).catch((error) => {
            if (onError) onError({
                code: 'SERVICE_RESPONSE',
                error
            });
        });

    },

    onFetchInfo(data, item, onError, onStart, onProgress, onFinish) {

        if (data.output && data.output.format && data.output.format.length) {

            if (data.output.format.length > 1) {

                return this.qualitySelecttor(data, item, onError, onStart, onProgress, onFinish)
            }

            let download = data.output.format[0].url;
            let fileTitleSlug = slugify(data.output.title, '_');
            let filename = Shared.libraryPath + '/' + fileTitleSlug + '.mp4';
            let thumbDownload = item.snippet.thumbnails.high.url;
            let thumbFilename = Shared.libraryPath + '/' + fileTitleSlug + '.jpg';
            let metaFilename = Shared.libraryPath + '/' + fileTitleSlug + '.json';

            // save meta
            RNFS.writeFile(metaFilename, JSON.stringify(item), 'utf8');

            // download thumb
            RNFS.downloadFile({
                fromUrl: thumbDownload,
                toFile: thumbFilename,
                background: false,
                progressDivider: 1
            });

            // download video

            RNFS.downloadFile({
                fromUrl: download.split("title")[0],
                toFile: filename,
                begin: onStart,
                progress: onProgress,
                background: true,
                progressDivider: 1
            }).promise.then(res => {
                if (onFinish) onFinish(res);
            });


        } else {


            if (onError) {

                this.offlineForce(item, onError, onStart, onProgress, onFinish, false)

            }

        }

    },

    isDownloaded(item, callBack) {
        let fileTitleSlug = slugify(item.snippet.title, '_');

        let fileName = fileTitleSlug + ".mp4";

        RNFS.exists(Shared.libraryPath + '/' + fileName).then((status) => {
            callBack(status)
        })
    },

    stopDownload(jobId) {

        Shared.onProgressFinish(jobId);
        return RNFS.stopDownload(jobId);

    },

    removeVideo(item, callBack) {
        let fileTitleSlug = slugify(item.snippet.title, '_');

        let videoFileName = fileTitleSlug + ".mp4";
        let metaFileName = fileTitleSlug + ".json";
        let thumbFileName = fileTitleSlug + ".jpg";

        let videoId = item.id.videoId ? item.id.videoId : item.id;

        Shared.clearVideoInfo(videoId);

        RNFS.unlink(Shared.libraryPath + '/' + videoFileName).then(() => {
            if (callBack) callBack();
        });
        RNFS.unlink(Shared.libraryPath + '/' + metaFileName);
        RNFS.unlink(Shared.libraryPath + '/' + thumbFileName);
    }

};
