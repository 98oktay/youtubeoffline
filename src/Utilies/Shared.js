import {Platform, AsyncStorage} from "react-native";
import RNFS from "react-native-fs";

let Shared = {

    reloadLibrary: () => {
    },
    switchMainTab: () => {
    },
    libraryPath: RNFS.DocumentDirectoryPath + '/videos',
    isDownloaded: {},
    YTDurationToSeconds: duration => {
        let match = duration.match(/PT(\d+H)?(\d+M)?(\d+S)?/);

        match = match.slice(1).map(function (x) {
            if (x != null) {
                return x.replace(/\D/, '');
            }
        });

        let hours = (parseInt(match[0]) || 0);
        let minutes = (parseInt(match[1]) || 0);
        let seconds = (parseInt(match[2]) || 0);

        return hours * 3600 + minutes * 60 + seconds;
    },

    durationBarUpdaters: [],
    expiryWorkUpdaters: [],

    registerDurationBarUpdater(key, callback) {
        this.durationBarUpdaters[key] = callback;
    },

    registerExpiryWorkUpdater(key, callback) {
        this.expiryWorkUpdaters[key] = callback;
    },

    callDurationBarUpdater(key) {

        if (this.durationBarUpdaters[key]) {
            this.durationBarUpdaters[key]();
        }
    },

    callExpiryWorkUpdater(key) {

        if (this.expiryWorkUpdaters[key]) {
            this.expiryWorkUpdaters[key]();
        }
    },

    clearVideoInfo(videoId, callback) {
        AsyncStorage.removeItem(videoId + "videoInfo").then(() => {
            if(callback) return callback(true)
        });
    },

    getVideoInfo(videoId, callback) {
        AsyncStorage.getItem(videoId + "videoInfo").then((data) => {

            if (data) {
                return callback(JSON.parse(data));
            }
            return callback({})
        });
    },
    setVideoInfo(videoId, data,  callback) {
        AsyncStorage.setItem(videoId + "videoInfo", JSON.stringify(data)).then(() => {
            return callback(true)
        });
    },
    onProgressUpdate() {},
    onProgressFinish() {}

};

export default Shared;